#!/usr/bin/env bash

# Hyperparameters
hidden=(20 50 100)
epochs=(50)
dropout_rate=(0.2 0.6 0.8)
learning_rate=(0.001 0.0001)
batch_size=(32)
optimizer=('SGD')

for e in ${epochs[@]};
do
    for b in ${batch_size[@]};
    do
        for h in ${hidden[@]};
        do
            for l in ${learning_rate[@]};
            do
                for d in ${dropout_rate[@]};
                do
                    for o in ${optimizer[@]};
                    do
                        echo python3 /home/dhrubo/rnn_expta1/GRU_UrbanSound8K.py /mnt/UrbanSound8K/logmelspec_48KHz/ $h $e $d $l $b $o 1
                    done
                done
            done
        done
    done
done
