from __future__ import print_function

import pickle
import numpy as np
import sys
import os

# Limit visibility to one GPU
#os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
if len(sys.argv) > 8:
    os.environ["CUDA_VISIBLE_DEVICES"]=sys.argv[8]
#else:
#    os.environ["CUDA_VISIBLE_DEVICES"] = '1' # Trying to avoid the CUDA_ERROR_ECC_UNCORRECTABLE for CUDA device ordinal 0 error on Prince

# Do not allocate all the memory for visible GPU
import tensorflow as tf
from keras import backend as K
config = tf.ConfigProto()
config.gpu_options.allow_growth=True
sess = tf.Session(config=config)
K.set_session(sess)

# Other imports
from keras.utils import Sequence, to_categorical
from keras.callbacks import ReduceLROnPlateau, EarlyStopping, ModelCheckpoint
from keras.regularizers import l1, l2
from keras.optimizers import Adam, SGD
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers.core import Dense
from keras.layers import Reshape, Dropout  # InputLayer
# from keras.layers.recurrent import LSTM
from keras.layers.recurrent import GRU
import h5py


class EarlyStoppingAfterNEpochs(EarlyStopping):
    def __init__(self, monitor='val_loss',
             min_delta=0, patience=0, verbose=0, mode='auto', start_epoch = 50): # add argument for starting epoch
        super(EarlyStoppingAfterNEpochs, self).__init__()
        self.start_epoch = start_epoch

    def on_epoch_end(self, epoch, logs=None):
        if epoch > self.start_epoch:
            super().on_epoch_end(epoch, logs)


# Input directory
in_path = sys.argv[1]
# Num classes
nb_classes = 10
np.random.seed(1337)  # for reproducibility
n_mels = 256
# Hidden units
hidden_units = int(sys.argv[2])  # 256
nb_epochs = int(sys.argv[3])  # 100
dropout_rate = float(sys.argv[4])  # 0.2
learning_rate = float(sys.argv[5])  # 1e-4
# Batch size
batch_size = int(sys.argv[6])  # 64
# Optimizer
if sys.argv[7].lower() == 'Adam'.lower():
    optimizer = Adam(lr=learning_rate)
elif sys.argv[7].lower() == 'SGD'.lower():
    optimizer = SGD(lr=learning_rate, momentum=0.9, nesterov=True)
# Stacked?
if len(sys.argv) > 9 and sys.argv[9].lower().__contains__('Stack'.lower()):  # Last argument can be stack/stacked/none
    stacked = True
    return_sequences = True
else:
    stacked = False
    return_sequences = False

# Dropout?
if dropout_rate == 0.0:
    dropout = True
else:
    dropout = False

# Output filenames
if stacked:
    history_fname = 'stackedgruhist_h=' + sys.argv[2] + '_e=' + sys.argv[3] + '_d=' + sys.argv[4] + '_l=' + sys.argv[
        5] + '_b=' + sys.argv[6] + '_' + sys.argv[7].lower() + '.pkl'
else:
    history_fname = 'gruhist_h=' + sys.argv[2] + '_e=' + sys.argv[3] + '_d=' + sys.argv[4] + '_l=' + sys.argv[
        5] + '_b=' + sys.argv[6] + '_' + sys.argv[7].lower() + '.pkl'

model_path = os.path.join(os.getcwd(),'model')
if not os.path.exists(model_path):
    os.makedirs(model_path)
model_file = os.path.join(model_path, history_fname.replace('pkl', 'h5py'))
print('MODEL PATH:', model_file)

# Add diagnostic line
print('OUTPUT FILENAME: ', history_fname)

# Open us8k.h5py
file = h5py.File(os.path.join(in_path,"us8k.hdf5"),'r')
#Extract mean and std
mean = file['mean'][:]
mean = mean[1:] # First element corresponds to label
std = file['std'][:]
std = std [1:] # First element corresponds to label
# Extract all training, validation, testing data
train_all = file['train']
validate_all = file['validate']
test_all = file['test']


class DataGenerator(Sequence):
    'Generates data for Keras'
    def __init__(self, type, batch_size=32, n_classes=10, shuffle=True):
        'Initialization'
        if type.lower().__contains__('train'):
            self.data = train_all
        elif type.lower().__contains__('val'):
            self.data = validate_all
        self.indexes = np.arange(self.data.shape[0])
        self.batch_size = batch_size
        self.n_classes = n_classes
        self.shuffle = shuffle
        self.on_epoch_end()
        self.data_dim = self.data.shape[1]-1 # excluding label

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(self.data.shape[0] / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        #print('index:', index)
        indexes = self.indexes[index * self.batch_size:(index + 1) * self.batch_size]
        #print('indexes', type(indexes))
        data_all = self.data[indexes,:]
        #print('data_all', data_all)
        # Split data and labels
        X = data_all[:, 1:]
        y = data_all[:, 0]
        # Standardize data
        X = (X - mean) / std
        # Convert label to one-hot
        y = to_categorical(y, num_classes=self.n_classes)

        return X, y


train_gen = DataGenerator(type='train', batch_size=batch_size, n_classes=nb_classes)
val_gen = DataGenerator(type='val', batch_size=batch_size, n_classes=nb_classes)

n_steps = train_gen.data_dim // n_mels
print('n_steps:', n_steps, 'input_dim', train_gen.data_dim)
# Initialize model
model = Sequential()
model.add(Reshape((-1, n_mels), input_shape=(train_gen.data_dim,)))
# model.add(LSTM(units=hidden_units, init='uniform', inner_init='uniform',
#            forget_bias_init='one', input_shape=(n_steps,n_mels)))
# model.add(LSTM(kernel_initializer="uniform", input_shape=(n_steps, n_mels),
#               recurrent_initializer="uniform", units=hidden_units, unit_forget_bias=True))
model.add(GRU(kernel_initializer="uniform", input_shape=(n_steps, n_mels),
              recurrent_initializer="uniform", units=hidden_units, return_sequences=return_sequences,
              kernel_regularizer=l2(0.01)))
# Add dropout layer (optional)
if dropout:
    model.add(Dropout(rate=dropout_rate, seed=1337))
# Add stacked layer (optional)
if stacked:  # TODO: Make stacked hidden unit different
    model.add(GRU(kernel_initializer="uniform", recurrent_initializer="uniform", units=hidden_units,
                  kernel_regularizer=l2(0.01)))
    # Add second dropout layer (optional)
    if dropout:  # TODO: Make stacked dropout rate different
        model.add(Dropout(rate=dropout_rate, seed=1337))
model.add(Dense(nb_classes, activation='softmax'))

model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])
model.summary()

# Begin training
print("Train...")

# Callbacks
reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.1,
                              patience=5, min_lr=0.000001, verbose=1)
early_stop = EarlyStoppingAfterNEpochs(monitor='val_loss', patience=10, verbose=1, start_epoch=50)
model_ckpt = ModelCheckpoint(model_file, monitor='val_loss', save_best_only=True, verbose=0)

# Fit generator
#history = model.fit(X_train, y_train, batch_size=batch_size, epochs=nb_epochs, validation_data=(X_val, y_val),
#                    callbacks=[reduce_lr])
history = model.fit_generator(generator=train_gen, validation_data=val_gen, callbacks=[reduce_lr, early_stop, model_ckpt], epochs=nb_epochs, use_multiprocessing=True,workers=6)
# Append params to history
# history.params["lr"] = learning_rate
# history.params["dr"] = dropout_rate
# history.params["n_hid"] = hidden_units
# history.params["opt"] = sys.argv[7]

# TODO: Uncomment testing
'''# Begin testing
score, acc = model.evaluate(X_test, y_test,
                            batch_size=batch_size)
print('Test score:', score)
print('Test accuracy:', acc)
# Append test details to history
history.history["test_loss"] = score
history.history["test_acc"] = acc'''

# Save history and params
with open(history_fname, 'wb') as file_pi:
    pickle.dump(history.history, file_pi)
# with open(params_fname, 'wb') as file_pi:
#    pickle.dump(history.params, file_pi)
