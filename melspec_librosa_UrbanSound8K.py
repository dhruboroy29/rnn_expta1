import librosa
import numpy as np
import soundfile as sf
import resampy
import csv
import sys
import os

NUM_FOLDS = 10
metadata = []


def load_us8k_metadata(path):
    """
    Load UrbanSound8K metadata
    Args:
        path: Path to metadata csv file
              (Type: str)
    Returns:
        metadata: List of metadata dictionaries
                  (Type: list[dict[str, *]])
    """
    metadata = [{} for _ in range(NUM_FOLDS)]
    with open(path) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            fname = row['slice_file_name']
            row['start'] = float(row['start'])
            row['end'] = float(row['end'])
            row['salience'] = float(row['salience'])
            fold_num = row['fold'] = int(row['fold'])
            row['classID'] = int(row['classID'])
            metadata[fold_num - 1][fname] = row

    return metadata


def compute_logmelspecs(fname, sr, class_label):
    logmelspec = []
    basename = os.path.basename(fname).split(os.extsep)[0]
    # output_path = os.path.join(output_dir, basename)

    audio, sr_orig = sf.read(fname, dtype='float32', always_2d=True)
    audio = audio.mean(axis=-1)

    if sr_orig != sr:
        audio = resampy.resample(audio, sr_orig, sr)

    hop_size = 0.1
    hop_length = int(hop_size * sr)
    frame_length = sr * 1

    audio_length = len(audio)
    if audio_length < frame_length:
        # Make sure we can have at least one frame of audio
        pad_length = frame_length - audio_length
    else:
        # Zero pad so we compute embedding on all samples
        pad_length = int(np.ceil(audio_length - frame_length) / hop_length) * hop_length \
                     - (audio_length - frame_length)

    if pad_length > 0:
        # Use (roughly) symmetric padding
        left_pad = pad_length // 2
        right_pad = pad_length - left_pad
        audio = np.pad(audio, (left_pad, right_pad), mode='constant')

    # Divide into overlapping 1 second frames
    x = librosa.util.utils.frame(audio, frame_length=frame_length, hop_length=hop_length).T

    # Get class label
    for row in range(np.shape(x)[0]):
        y = x[row]
        # Compute log-melspecs on each window
        n_fft = 2048
        # n_win = 480
        # n_hop = n_win//2
        n_mels = 256
        n_hop = 242
        S = librosa.feature.melspectrogram(y, sr=sr, n_fft=n_fft, n_mels=n_mels, hop_length=n_hop).T
        # Convert to log scale (dB). We'll use the peak power as reference.
        log_S = librosa.power_to_db(S, ref=np.max)

        # Reshape as a 1-D array
        log_S = log_S.ravel()
        # Concatenate class label as first element
        log_S = np.concatenate((class_label, log_S))

        logmelspec.append(log_S)

    return logmelspec
    # np.savez_compressed(output_path, X=logmelspec, y=class_label)
    # Test read
    # read = np.load(output_path+'.npy')
    # print('ReadTest')


def compute_logstft(fname, sr, class_label):
    logstft = []
    basename = os.path.basename(fname).split(os.extsep)[0]
    # output_path = os.path.join(output_dir, basename)

    audio, sr_orig = sf.read(fname, dtype='float32', always_2d=True)
    audio = audio.mean(axis=-1)

    if sr_orig != sr:
        audio = resampy.resample(audio, sr_orig, sr)

    hop_size = 0.1
    hop_length = int(hop_size * sr)
    frame_length = sr * 1

    audio_length = len(audio)
    if audio_length < frame_length:
        # Make sure we can have at least one frame of audio
        pad_length = frame_length - audio_length
    else:
        # Zero pad so we compute embedding on all samples
        pad_length = int(np.ceil(audio_length - frame_length) / hop_length) * hop_length \
                     - (audio_length - frame_length)

    if pad_length > 0:
        # Use (roughly) symmetric padding
        left_pad = pad_length // 2
        right_pad = pad_length - left_pad
        audio = np.pad(audio, (left_pad, right_pad), mode='constant')

    # Divide into overlapping 1 second frames
    x = librosa.util.utils.frame(audio, frame_length=frame_length, hop_length=hop_length).T

    # Get class label
    for row in range(np.shape(x)[0]):
        y = x[row]
        # Compute log-melspecs on each window
        n_fft = 510 #2048
        # n_win = 480
        # n_hop = n_win//2
        #win_length = 256
        n_hop = 242
        S = librosa.core.stft(y, n_fft=n_fft, hop_length=n_hop).T
        # Convert to log scale (dB). We'll use the peak power as reference.
        log_S = librosa.amplitude_to_db(S, ref=np.max)

        # Reshape as a 1-D array
        log_S = log_S.ravel()
        # Concatenate class label as first element
        log_S = np.concatenate((class_label, log_S))

        logstft.append(log_S)

    return logstft


if __name__ == "__main__":
    basepath = '/scratch/dr2915/UrbanSound8K/audio'
    fold_idx = int(sys.argv[1])
    # Compute melspec or stft; default to melspec
    if len(sys.argv) < 3:
        spectype = 'mel'
    else:
        spectype = sys.argv[2]

    target_sr = 48000
    if spectype.__contains__('mel'):
        out_path = os.path.join(basepath.replace('audio', 'logmelspec_' + str(target_sr // 1000) + 'KHz'),
                            'fold' + str(fold_idx + 1))
    elif spectype.__contains__('stft'):
        out_path = os.path.join(basepath.replace('audio', 'logstft_' + str(target_sr // 1000) + 'KHz'),
                                'fold' + str(fold_idx + 1))
    if not os.path.exists(out_path):
        os.makedirs(out_path)

    metadata = load_us8k_metadata(os.path.join('/scratch/dr2915/UrbanSound8K/metadata/UrbanSound8K.csv'))

    print('Input directory:', basepath, '\n')

    # Compute logmelspecs
    # for fold_idx in range(NUM_FOLDS):
    for idx, (file, example_metadata) in enumerate(metadata[fold_idx].items()):
        print('Processing fold: ', str(fold_idx + 1), 'file: ',
              os.path.join(basepath, 'fold' + str(fold_idx + 1), file))
        label = np.array([example_metadata['classID']])
        # Compute melspec or stft
        if spectype.__contains__('mel'):
            spec = compute_logmelspecs(os.path.join(basepath, 'fold' + str(fold_idx + 1), file), target_sr, label)
        elif spectype.__contains__('stft'):
            spec = compute_logstft(os.path.join(basepath, 'fold' + str(fold_idx + 1), file), target_sr, label)
        # Save file
        outfile = os.path.basename(file).split(os.extsep)[0]
        np.save(os.path.join(out_path, outfile), spec)
