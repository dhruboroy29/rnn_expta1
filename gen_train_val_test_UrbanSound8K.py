import numpy as np
import os
import sys
import h5py

in_path = sys.argv[1] # '/scratch/dr2915/UrbanSound8K/logmelspec_48KHz'
file_type = sys.argv[2] # 'train' or 'valtest'
all_folds = os.listdir(in_path)
# TODO: Remove hard-coding
val_fold = 8
test_fold = 9

if len(sys.argv)>3 and sys.argv[3]=='h5py':
    out_type = 'hdf5'
    hf = h5py.File(os.path.join(in_path, 'us8k.hdf5'), 'a')
else:
    out_type = 'npy'

list_files = []
for fold in all_folds:
    if not fold.__contains__('fold'):
        continue

    files = [os.path.join(in_path,fold,file) for file in os.listdir(os.path.join(in_path,fold))]
    list_files.append(files)
# Flatten
list_files = [item for sublist in list_files for item in sublist]

# Shuffle all
np.random.seed(20180302)
np.random.shuffle(list_files)

tr_dat = []
val_dat = []
test_dat = []

for file in list_files:
    print('Processing file: ', file)
    dat = np.load(file)
    if file_type.__contains__('train'):
        if file.__contains__('fold' + str(val_fold)) or file.__contains__('fold'+str(test_fold)):
            continue
        else:
            tr_dat.append(dat)
    else:
        if file.__contains__('fold'+str(val_fold)):
            val_dat.append(dat)
        elif file.__contains__('fold'+str(test_fold)):
            test_dat.append(dat)
        else:
            continue

if file_type.__contains__('train'):
    tr_dat = np.concatenate(tr_dat)
    # Compute training mean and std
    mean = np.mean(tr_dat, 0)
    std = np.std(tr_dat, 0)
    std[std[:] < 0.00001] = 1

    if out_type=='npy':
        np.save(os.path.join(in_path, 'train'), tr_dat, allow_pickle=False)
        np.save(os.path.join(in_path, 'mean'), mean, allow_pickle=False)
        np.save(os.path.join(in_path, 'std'), std, allow_pickle=False)
    elif out_type=='hdf5':
        hf['train'] = tr_dat
        hf['mean'] = mean
        hf['std'] = std

        hf.close()
else:
    val_dat = np.concatenate(val_dat)
    test_dat = np.concatenate(test_dat)

    if out_type == 'npy':
        np.save(os.path.join(in_path, 'validate'), val_dat, allow_pickle=False)
        np.save(os.path.join(in_path, 'test'), test_dat, allow_pickle=False)
    elif out_type == 'hdf5':
        hf['validate'] = val_dat
        hf['test'] = test_dat

        hf.close()

# Generate train-validate-test partitions
#train_prop = 0.80
#val_prop = 0.10
#test_prop = 0.10

# Save training, testing and validation data
#train_data = tr_dat[:int(train_prop*len(tr_dat))]
#val_data = tr_dat[int(train_prop*len(tr_dat)):int((train_prop+val_prop)*len(tr_dat))]
#test_data = tr_dat[int((train_prop+val_prop)*len(tr_dat)):]