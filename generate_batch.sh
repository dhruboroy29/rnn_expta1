#!/usr/bin/env bash

rnn="gru"

# Hyperparameters
hidden=(100 200 256)
epochs=(300)
dropout_rate=(0.5 0.8)
learning_rate=(0.01 0.001 0.0001)
batch_size=(100 64 32)
optimizer=('Adam')

for e in ${epochs[@]};
do
    for b in ${batch_size[@]};
    do
        for h in ${hidden[@]};
        do
            for l in ${learning_rate[@]};
            do
                for d in ${dropout_rate[@]};
                do
                    for o in ${optimizer[@]};
                    do
                        echo sbatch /home/'$USER'/rnn_expta1/${rnn}_dcase2013.sbatch $h $e $d $l $b $o
                        echo 'sleep 1'
                    done
                done
            done
        done
    done
done
