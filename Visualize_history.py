import matplotlib.pyplot as plt
import numpy as np
import os
import sys
import glob
from matplotlib.backends.backend_pdf import PdfPages

# create a PdfPages object
in_path = sys.argv[1] #'/Users/Balderdash/Downloads/stacked_gru'
if in_path.__contains__('.pkl'):
    base_name = os.path.basename(in_path).strip('.pkl').replace('*', 'all')
    files = glob.glob(in_path)
else:
    base_name = os.path.basename(in_path)
    files = glob.glob(os.path.join(in_path,'*.pkl'))

with PdfPages('accloss_'+ base_name + '.pdf') as pdf_acc:
    for file in files:
        hist_fname = os.path.basename(file)
        params = hist_fname[hist_fname.index('h='):hist_fname.index('.pkl')]
        prefix = hist_fname[:hist_fname.index('hist')]

        #if in_path.lower().__contains__('stack'):
        #    suffix = 'stacked'
        #else:
        #    suffix = 'simple'

        history = np.load(file)

        #fig, axes = plt.subplots(nrows=1, ncols=2)
        plt.subplot(211)
        # summarize history for accuracy
        #fig = plt.figure()
        plt.plot(history['acc'])
        plt.plot(history['val_acc'])
        plt.title(prefix + '_' + params)
        #plt.title('Accuracy (' + params + ')')
        plt.ylabel('Accuracy')
        #plt.xlabel('Epoch')
        plt.legend(['Train', 'Validation'], loc='upper left')

        plt.subplot(212)
        plt.plot(history['loss'])
        plt.plot(history['val_loss'])
        #plt.title('Loss (' + params + ')')
        plt.ylabel('Loss')
        plt.xlabel('Epoch')
        plt.legend(['Train', 'Validation'], loc='upper left')
        pdf_acc.savefig()
        plt.close()