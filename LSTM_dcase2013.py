from __future__ import print_function

import pickle
import numpy as np
import sys
import os

from keras.optimizers import Adam, SGD
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers.core import Dense
from keras.layers import Reshape, Dropout #InputLayer
from keras.layers.recurrent import LSTM
#from keras.layers.recurrent import GRU

# Input directory
in_path = sys.argv[1]
# Num classes
nb_classes = 10
np.random.seed(1337)  # for reproducibility
n_mels = 256
# Hidden units
hidden_units = int(sys.argv[2])  # 256
nb_epochs = int(sys.argv[3])  # 100
dropout_rate = float(sys.argv[4])  # 0.2
learning_rate = float(sys.argv[5])  # 1e-4
# Batch size
batch_size = int(sys.argv[6])  # 64
# Optimizer
if sys.argv[7].lower() == 'Adam'.lower():
    optimizer = Adam(lr=learning_rate)
elif sys.argv[7].lower() == 'SGD'.lower():
    optimizer = SGD(lr=learning_rate, momentum=0.9, nesterov=True)

# Output filenames
history_fname = 'lstmhist_h=' + sys.argv[2] + '_e=' + sys.argv[3] + '_d=' + sys.argv[4] + '_l=' + sys.argv[
    5] + '_b=' + sys.argv[6] + '_' + sys.argv[7].lower() + '.pkl'
params_fname = 'lstmparams_h=' + sys.argv[2] + '_e=' + sys.argv[3] + '_d=' + sys.argv[4] + '_l=' + sys.argv[
    5] + '_b=' + sys.argv[6] + '_' + sys.argv[7].lower() + '.pkl'

# Load data
train_all = np.load(os.path.join(in_path, 'train.npy'))
validate_all = np.load(os.path.join(in_path, 'validate.npy'))
test_all = np.load(os.path.join(in_path, 'test.npy'))

# Get train data
X_train = train_all[:,1:]
y_train = train_all[:,0]
y_train = np_utils.to_categorical(y_train, nb_classes)

# Get validation data
X_val = validate_all[:,1:]
y_val = validate_all[:,0]
y_val = np_utils.to_categorical(y_val, nb_classes)

# Get test data
X_test = test_all[:,1:]
y_test = test_all[:,0]
y_test = np_utils.to_categorical(y_test, nb_classes)


# Standardize data
mean=np.mean(X_train,0)
std=np.std(X_train,0)
std[std[:]<0.00001]=1
X_train=(X_train-mean)/std
X_val=(X_val-mean)/std
X_test=(X_test-mean)/std

n_steps = X_train.shape[1] // n_mels

# Initialize model
model = Sequential()
model.add(Reshape((-1,n_mels),input_shape=(X_train.shape[1],)))
#model.add(LSTM(units=hidden_units, init='uniform', inner_init='uniform',
#            forget_bias_init='one', input_shape=(n_steps,n_mels)))
model.add(LSTM(kernel_initializer="uniform", input_shape=(n_steps, n_mels),
               recurrent_initializer="uniform", units=hidden_units, unit_forget_bias=True))
#model.add(GRU(kernel_initializer="uniform", input_shape=(n_steps, n_mels),
#               recurrent_initializer="uniform", units=hidden_units))
model.add(Dropout(rate=dropout_rate, seed=1337))
model.add(Dense(nb_classes, activation='softmax'))

model.compile(optimizer=optimizer,
              loss='categorical_crossentropy', metrics=['accuracy'])
model.summary()


# Begin training
print("Train...")
history = model.fit(X_train, y_train, batch_size=batch_size, epochs=nb_epochs, validation_data=(X_val, y_val))
# Append params to history
history.params["lr"] = learning_rate
history.params["dr"] = dropout_rate
history.params["n_hid"] = hidden_units
history.params["opt"] = sys.argv[7]

# Begin testing
score, acc = model.evaluate(X_test, y_test,
                            batch_size=batch_size)
print('Test score:', score)
print('Test accuracy:', acc)
# Append test details to history
history.history["test_loss"] = score
history.history["test_acc"] = acc

# Save history and params
with open(history_fname, 'wb') as file_pi:
    pickle.dump(history.history, file_pi)
with open(params_fname, 'wb') as file_pi:
    pickle.dump(history.params, file_pi)
