import librosa
import numpy as np
import soundfile as sf
import resampy
import sys
import os
#import progressbar

CLASS_TO_INT = {
    'bus': 0,
    'busystreet': 1,
    'office': 2,
    'openairmarket': 3,
    'park': 4,
    'quietstreet': 5,
    'restaurant': 6,
    'supermarket': 7,
    'tube': 8,
    'tubestation': 9
}

logmelspec = []

def compute_logmelspecs(fname, sr):
    basename = os.path.basename(fname).split(os.extsep)[0]
    #output_path = os.path.join(output_dir, basename)

    audio, sr_orig = sf.read(fname, dtype='float32', always_2d=True)
    audio = audio.mean(axis=-1)

    if sr_orig != sr:
        audio = resampy.resample(audio, sr_orig, sr)

    hop_size = 0.1
    hop_length = int(hop_size * sr)
    frame_length = sr * 1

    audio_length = len(audio)
    if audio_length < frame_length:
        # Make sure we can have at least one frame of audio
        pad_length = frame_length - audio_length
    else:
        # Zero pad so we compute embedding on all samples
        pad_length = int(np.ceil(audio_length - frame_length)/hop_length) * hop_length \
                     - (audio_length - frame_length)

    if pad_length > 0:
        # Use (roughly) symmetric padding
        left_pad = pad_length // 2
        right_pad= pad_length - left_pad
        audio = np.pad(audio, (left_pad, right_pad), mode='constant')

    # Divide into overlapping 1 second frames
    x = librosa.util.utils.frame(audio, frame_length=frame_length, hop_length=hop_length).T

    # Get class label
    class_label = np.array([CLASS_TO_INT[basename[:-2]]])
    for row in range(np.shape(x)[0]):
        y=x[row]
        #Compute log-melspecs on each window
        n_fft = 2048
        # n_win = 480
        # n_hop = n_win//2
        n_mels = 256
        n_hop = 242
        S = librosa.feature.melspectrogram(y, sr=sr, n_fft=n_fft, n_mels=n_mels, hop_length=n_hop).T
        # Convert to log scale (dB). We'll use the peak power as reference.
        log_S = librosa.power_to_db(S, ref=np.max)

        # Reshape as a 1-D array
        log_S = log_S.ravel()
        # Concatenate class label as first element
        log_S = np.concatenate((class_label,log_S))

        logmelspec.append(log_S)

    #np.savez_compressed(output_path, X=logmelspec, y=class_label)
    # Test read
    #read = np.load(output_path+'.npy')
    #print('ReadTest')


if __name__=="__main__":
    train_path = '/scratch/dr2915/dcase2013/audio/fold1'
    test_path = '/scratch/dr2915/dcase2013/audio/fold2'
    target_sr = 48000
    out_path = train_path.replace('audio', 'logmelspec_' + str(target_sr // 1000) + 'KHz')
    out_path = out_path[:out_path.index('/fold')]
    out_path = os.path.join(out_path,'data')
    if not os.path.exists(out_path):
        os.makedirs(out_path)

    audio_files = os.listdir(train_path)
    np.random.shuffle(audio_files)

    print('Input directory:', train_path, '\n')
    #bar = progressbar.ProgressBar(maxval=len(audio_files), \
    #                              widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
    #bar.start()
    #n_iter=1

    # Generate train-test-validate partitions
    train_prop = 0.85
    #val_prop = 0.2
    #test_prop = 0.3

    audio_train = audio_files[:int(train_prop*len(audio_files))]
    audio_val = audio_files[int(train_prop*len(audio_files)):]
    #audio_test = audio_files[int((train_prop+val_prop)*len(audio_files)):]
    audio_test = os.listdir(test_path)

    print('PROCESSING TRAINING DATA')
    # Compute log-melspecs in each slice - training
    for file in audio_train:
        print('Processing ', file)
        compute_logmelspecs(os.path.join(train_path, file), target_sr)
        #bar.update(n_iter)
        #n_iter += 1
    # Save a giant data file
    np.save(os.path.join(out_path,'train'),logmelspec)
    
    logmelspec = []
    print('PROCESSING VALIDATION DATA')
    # Compute log-melspecs in each slice - validation
    for file in audio_val:
        print('Processing ', file)
        compute_logmelspecs(os.path.join(train_path, file), target_sr)
        #bar.update(n_iter)
        #n_iter += 1
    # Save a giant data file
    np.save(os.path.join(out_path, 'validate'), logmelspec)

    print('Input directory:', test_path, '\n')
    logmelspec = []
    # Compute log-melspecs in each slice - testing
    print('PROCESSING TEST DATA')
    for file in audio_test:
        print('Processing ', file)
        compute_logmelspecs(os.path.join(test_path, file), target_sr)
        #bar.update(n_iter)
        #n_iter += 1
    # Save a giant data file
    np.save(os.path.join(out_path, 'test'), logmelspec)

    #bar.finish()
